package ru.fadeev.tm.exception;

public final class IllegalProjectNameException extends RuntimeException {

    public IllegalProjectNameException(final String message) {
        super(message);
    }

}
