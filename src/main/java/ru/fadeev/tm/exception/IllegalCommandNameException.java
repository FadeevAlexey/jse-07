package ru.fadeev.tm.exception;

public final class IllegalCommandNameException extends RuntimeException {

    public IllegalCommandNameException(final String message) {
        super(message);
    }

}
