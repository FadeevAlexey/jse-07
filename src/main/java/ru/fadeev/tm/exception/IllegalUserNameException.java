package ru.fadeev.tm.exception;

public final class IllegalUserNameException extends RuntimeException {

    public IllegalUserNameException(final String message) {
        super(message);
    }

}
