package ru.fadeev.tm.exception;

public final class IllegalTaskNameException extends RuntimeException {

    public IllegalTaskNameException(final String message) {
        super(message);
    }

}
