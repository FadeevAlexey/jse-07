package ru.fadeev.tm.exception;

public final class IllegalUserPasswordException extends RuntimeException {

    public IllegalUserPasswordException(final String message) {
        super(message);
    }

}
