package ru.fadeev.tm.repository;

import ru.fadeev.tm.api.repository.IUserRepository;
import ru.fadeev.tm.entity.User;

public final class UserRepository extends AbstractRepository<User>  implements IUserRepository {

    @Override
    public boolean isLoginExist(final String login){
      return findAll().stream()
                .anyMatch(user -> user.getName().equals(login));
    }

}