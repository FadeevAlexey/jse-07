package ru.fadeev.tm.repository;

import ru.fadeev.tm.api.repository.IRepository;
import ru.fadeev.tm.entity.AbstractEntity;
import ru.fadeev.tm.exception.EntityDuplicateException;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    private final Map<String, E> entities = new LinkedHashMap<>();

    @Override
    public List<E> findAll() {
        return new LinkedList<>(entities.values());
    }

    @Override
    public E findOne(final String id) {
        return entities.get(id);
    }

    @Override
    public E remove(final String id) {
        return entities.remove(id);
    }

    @Override
    public E persist(final E e) {
        if (entities.containsKey(e.getId()))
            throw new EntityDuplicateException();
        return entities.put(e.getId(), e);
    }

    @Override
    public E merge(final E e) {
        return entities.put(e.getId(), e);
    }

    @Override
    public void removeAll() {
        entities.clear();
    }

}