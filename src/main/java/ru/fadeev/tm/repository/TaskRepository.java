package ru.fadeev.tm.repository;

import ru.fadeev.tm.api.repository.ITaskRepository;
import ru.fadeev.tm.entity.Task;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public void removeAll(final String userId) {
        findAll().stream()
                .filter(task -> task.getUserId().equals(userId))
                .forEach(task -> remove(task.getId()));
    }

    @Override
    public Collection<Task> findAll(final String userId) {
        return findAll().stream()
                .filter(task -> task.getUserId().equals(userId))
                .collect(Collectors.toList());
    }

    @Override
    public Collection<Task> findAllByProjectId(final String projectId) {
        return findAll().stream()
                .filter(task -> task.getUserId().equals(projectId))
                .collect(Collectors.toList());
    }

    @Override
    public String findIdByName(final String name, final String userId) {
        final Optional<Task> optionalProject = super.findAll()
                .stream()
                .filter(task ->
                        task.getName().equals(name) && task.getUserId().equals(userId))
                .findAny();
        return optionalProject.map(Task::getId).orElse(null);
    }

}