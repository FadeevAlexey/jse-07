package ru.fadeev.tm.repository;

import ru.fadeev.tm.api.repository.IProjectRepository;
import ru.fadeev.tm.entity.Project;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public void removeAll(final String userId) {
        findAll().stream()
                .filter(project -> project.getUserId().equals(userId))
                .forEach(project -> remove(project.getId()));
    }

    @Override
    public List<Project> findAll(final String userId){
      return findAll().stream()
                .filter(project -> project.getUserId().equals(userId))
                .collect(Collectors.toList());
    }

    @Override
    public String findIdByName(final String name, final String userId) {
        final Optional<Project> optionalProject = super.findAll()
                .stream()
                .filter(project ->
                        project.getName().equals(name) && project.getUserId().equals(userId))
                .findAny();
        return optionalProject.map(Project::getId).orElse(null);
    }

}