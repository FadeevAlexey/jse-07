package ru.fadeev.tm.command;

import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;

public abstract class AbstractCommand {

    protected final ServiceLocator serviceLocator;

    public AbstractCommand(final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public boolean isPermission(final User user) {
        return user != null;
    }

    public abstract void execute();

    public abstract String getName();

    public abstract String getDescription();

    public Role[] accessRole(){
        return null;
    }

}