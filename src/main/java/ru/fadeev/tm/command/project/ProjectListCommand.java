package ru.fadeev.tm.command.project;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.Project;

public final class ProjectListCommand extends AbstractCommand {

    public ProjectListCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() {
        final String currentUserId = serviceLocator.getAppStateService().getUser().getId();
        System.out.println("[PROJECT LIST]");
        int index = 1;
        for (final Project project : serviceLocator.getProjectService().findAll(currentUserId))
            System.out.println(index++ + ". " + project);
        System.out.println();
    }

}