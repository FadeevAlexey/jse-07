package ru.fadeev.tm.command.project;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.exception.IllegalProjectNameException;
import ru.fadeev.tm.api.service.IProjectService;
import ru.fadeev.tm.util.ConsoleHelper;

public final class ProjectRemoveCommand extends AbstractCommand {

    public ProjectRemoveCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "project-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected project.";
    }

    @Override
    public void execute() {
        final IProjectService projectService = serviceLocator.getProjectService();
        final String currentUserId = serviceLocator.getAppStateService().getUser().getId();
        System.out.println("[PROJECT REMOVE]\nENTER NAME");
        final String projectId = projectService.findIdByName(ConsoleHelper.readString(), currentUserId);
        if (projectId == null)
            throw new IllegalProjectNameException("Can't find project");
        projectService.remove(projectId);
        serviceLocator.getTaskService().removeAllByProjectId(projectId, currentUserId);
        System.out.println("[OK]\n");
    }

}