package ru.fadeev.tm.command.project;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.Project;
import ru.fadeev.tm.exception.IllegalProjectNameException;
import ru.fadeev.tm.api.service.IProjectService;
import ru.fadeev.tm.util.ConsoleHelper;

import java.util.Date;

public final class ProjectEditCommand extends AbstractCommand {

    public ProjectEditCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "project-edit";
    }

    @Override
    public String getDescription() {
        return "Edit project.";
    }

    @Override
    public void execute() {
        final IProjectService projectService = serviceLocator.getProjectService();
        final String currentUserId = serviceLocator.getAppStateService().getUser().getId();
        System.out.println("[EDIT PROJECT]");
        System.out.println("ENTER CURRENT NAME:");
        final String name = ConsoleHelper.readString();
        final String projectId = projectService.findIdByName(name, currentUserId);
        if (projectId == null)
            throw new IllegalProjectNameException("Can't find project with name" + name);
        final Project project = projectService.findOne(projectId);
        fillFields(project);
        projectService.merge(project);
        System.out.println("[OK]\n");
    }

    private void fillFields(final Project project) {
        System.out.println("YOU CAN ADD EDIT DESCRIPTION OR PRESS ENTER");
        final String description = ConsoleHelper.readString();
        System.out.println("YOU CAN ADD EDIT START DATE OR PRESS ENTER");
        final Date startDate = ConsoleHelper.readDate();
        System.out.println("YOU CAN ADD EDIT FINISH DATE OR PRESS ENTER");
        final Date finishDate = ConsoleHelper.readDate();
        if (description != null && !description.isEmpty()) project.setDescription(description);
        if (startDate != null) project.setStartDate(startDate);
        if (finishDate != null) project.setFinishDate(finishDate);
    }

}