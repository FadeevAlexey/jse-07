package ru.fadeev.tm.command.project;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.api.service.ServiceLocator;

public final class ProjectClearCommand extends AbstractCommand {

    public ProjectClearCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

    @Override
    public void execute() {
        final String currentUserId = serviceLocator.getAppStateService().getUser().getId();
        serviceLocator.getProjectService().removeAll(currentUserId);
        serviceLocator.getTaskService().removeAllProjectTask(currentUserId);
        System.out.println("[ALL PROJECTS REMOVE]\n");
    }

}