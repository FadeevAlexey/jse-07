package ru.fadeev.tm.command.project;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.exception.IllegalProjectNameException;
import ru.fadeev.tm.api.service.IProjectService;
import ru.fadeev.tm.api.service.ITaskService;
import ru.fadeev.tm.util.ConsoleHelper;

public final class ProjectTasksCommand extends AbstractCommand {

    public ProjectTasksCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "project-tasks";
    }

    @Override
    public String getDescription() {
        return "Show all tasks inside project.";
    }

    @Override
    public void execute() {
        final IProjectService projectService = serviceLocator.getProjectService();
        final ITaskService taskService = serviceLocator.getTaskService();
        final String currentUserId =  serviceLocator.getAppStateService().getUser().getId();
        System.out.println("[PROJECT TASKS]");
        System.out.println("ENTER PROJECT NAME");
        final String projectId = projectService.findIdByName(ConsoleHelper.readString(), currentUserId);
        if (projectId == null)
            throw new IllegalProjectNameException("Can't find project");
        int i = 1;
        for (final Task task : taskService.findAll(currentUserId)) {
            if (task.getProjectId().equals(projectId))
                System.out.println(i++ + ". " + task);
        }
        System.out.println();
    }

}