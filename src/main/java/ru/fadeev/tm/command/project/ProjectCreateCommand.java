package ru.fadeev.tm.command.project;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.Project;
import ru.fadeev.tm.exception.IllegalProjectNameException;
import ru.fadeev.tm.util.ConsoleHelper;

public final class ProjectCreateCommand extends AbstractCommand {

    public ProjectCreateCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "project-create";
    }

    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAppStateService().getUser().getId();
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        final String name = ConsoleHelper.readString();
        if (name == null || name.isEmpty())
            throw new IllegalProjectNameException("name can't be empty");
        final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        serviceLocator.getProjectService().persist(project);
        System.out.println("[OK]\n");
        System.out.println("WOULD YOU LIKE EDIT PROPERTIES PROJECT ? USE COMMAND project-edit\n");
    }

}