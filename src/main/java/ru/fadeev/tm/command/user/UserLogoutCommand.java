package ru.fadeev.tm.command.user;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.api.service.ServiceLocator;

public final class UserLogoutCommand extends AbstractCommand {

    public UserLogoutCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "user-logout";
    }

    @Override
    public String getDescription() {
        return "logout from task-manager";
    }

    @Override
    public void execute() {
        serviceLocator.getAppStateService().setUser(null);
        System.out.println("[OK]\n");
    }

}