package ru.fadeev.tm.command.user;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.exception.IllegalUserPasswordException;
import ru.fadeev.tm.util.HashUtil;
import ru.fadeev.tm.util.ConsoleHelper;

public final class UserUpdatePasswordCommand extends AbstractCommand {

    public UserUpdatePasswordCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "user-updatePassword";
    }

    @Override
    public String getDescription() {
        return "User Password Changes.";
    }

    @Override
    public void execute() {
        final User currentUser = serviceLocator.getAppStateService().getUser();
        System.out.println("[UPDATE PASSWORD]");
        System.out.println("Enter your current password");
        final String currentPassword = HashUtil.stringToMd5Hash(ConsoleHelper.readString());
        if (!currentUser.getPassword().equals(currentPassword))
            throw new IllegalUserPasswordException("wrong password");
        System.out.println("Enter new password");
        final String newPassword = HashUtil.stringToMd5Hash(ConsoleHelper.readString());
        currentUser.setPassword(newPassword);
        serviceLocator.getUserService().merge(currentUser);
        System.out.println("[OK]\n");
    }

}