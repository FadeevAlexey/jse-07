package ru.fadeev.tm.command.user;

import ru.fadeev.tm.api.service.IUserService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.exception.IllegalUserNameException;
import ru.fadeev.tm.exception.IllegalUserPasswordException;
import ru.fadeev.tm.util.HashUtil;
import ru.fadeev.tm.util.ConsoleHelper;

public final class UserLoginCommand extends AbstractCommand {

    public UserLoginCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean isPermission(final User user) {
        return true;
    }

    @Override
    public String getName() {
        return "user-login";
    }

    @Override
    public String getDescription() {
        return "log in task manager";
    }

    @Override
    public void execute() {
        final IUserService userService = serviceLocator.getUserService();
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN");
        String name = ConsoleHelper.readString();
        boolean loginExist = userService.isLoginExist(name);
        if (!loginExist)
            throw new IllegalUserNameException("Can't find user");
        final User user = userService.findUserByLogin(name);
        System.out.println("ENTER PASSWORD");
        final String userPassword = HashUtil.stringToMd5Hash(ConsoleHelper.readString());
        if (!userPassword.equals(user.getPassword()))
            throw new IllegalUserPasswordException("wrong password, access denied");
        serviceLocator.getAppStateService().setUser(user);
        System.out.println("[OK]\n");
    }

}