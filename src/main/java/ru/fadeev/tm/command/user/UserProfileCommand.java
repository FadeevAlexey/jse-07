package ru.fadeev.tm.command.user;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.api.service.ServiceLocator;

public final class UserProfileCommand extends AbstractCommand {

    public UserProfileCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "user-profile";
    }

    @Override
    public String getDescription() {
        return "Show current profile.";
    }

    @Override
    public void execute() {
        System.out.println("[USER PROFILE]");
        System.out.println(serviceLocator.getAppStateService().getUser() + "\n");
        System.out.println("IF YOU'D LIKE UPDATE PROFILE USE COMMAND: user-edit\n");
    }

}
