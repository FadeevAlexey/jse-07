package ru.fadeev.tm.command.user;

import ru.fadeev.tm.api.service.IUserService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;
import ru.fadeev.tm.exception.IllegalUserNameException;
import ru.fadeev.tm.util.HashUtil;
import ru.fadeev.tm.util.ConsoleHelper;

public final class UserCreateCommand extends AbstractCommand {

    public UserCreateCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean isPermission(final User user) {
        return true;
    }

    @Override
    public String getName() {
        return "user-create";
    }

    @Override
    public String getDescription() {
        return "creates a new user account";
    }

    @Override
    public void execute() {
        final User currentUser = serviceLocator.getAppStateService().getUser();
        final boolean isAdministrator = currentUser != null && Role.ADMINISTRATOR == currentUser.getRole();
        final IUserService userService = serviceLocator.getUserService();
        System.out.println("[CREATE ACCOUNT]");
        System.out.println("ENTER NAME");
        final String login = ConsoleHelper.readString();
        boolean isLoginExist = userService.isLoginExist(login);
        if (login == null || login.isEmpty())
            throw new IllegalUserNameException("Incorrect name");
        if (isLoginExist)
            throw new IllegalUserNameException("User with same name already exist");
        final User user = new User(login);
        System.out.println("Enter password:");
        user.setPassword(HashUtil.stringToMd5Hash(ConsoleHelper.readString()));
        if (isAdministrator)
            setRole(user);
        userService.persist(user);
        System.out.println("[OK]\n");
    }

    public void setRole(final User user) {
        System.out.println("would you like give new account administrator rights? y/n");
        if (ConsoleHelper.readString().equals("y"))
            user.setRole(Role.ADMINISTRATOR);
    }

}