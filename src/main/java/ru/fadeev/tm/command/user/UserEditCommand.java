package ru.fadeev.tm.command.user;

import ru.fadeev.tm.api.service.IUserService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.exception.IllegalUserNameException;
import ru.fadeev.tm.exception.IllegalUserPasswordException;
import ru.fadeev.tm.util.HashUtil;
import ru.fadeev.tm.util.ConsoleHelper;

public final class UserEditCommand extends AbstractCommand {

    public UserEditCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "user-edit";
    }

    @Override
    public String getDescription() {
        return "Edit user profile";
    }

    @Override
    public void execute() {
        final IUserService userService = serviceLocator.getUserService();
        User currentUser = serviceLocator.getAppStateService().getUser();
        System.out.println("[EDIT PROFILE]");
        System.out.println("ENTER PASSWORD");
        final String password = HashUtil.stringToMd5Hash(ConsoleHelper.readString());
        if (!password.equals(currentUser.getPassword()))
            throw new IllegalUserPasswordException("wrong password, access denied");
        System.out.println("ENTER NEW NAME OR PRESS ENTER");
        final String name = ConsoleHelper.readString();
        if(userService.isLoginExist(name))
            throw new IllegalUserNameException("User with same name already exist");
        System.out.println("ENTER NEW PASSWORD OR PRESS ENTER");
        final String newPassword = ConsoleHelper.readString();
        final User user = userService.findOne(currentUser.getId());
        if (newPassword != null && !newPassword.isEmpty()) user.setPassword(HashUtil.stringToMd5Hash(newPassword));
        if (name != null && !name.isEmpty()) user.setName(name);
        userService.merge(user);
        System.out.println("[OK]\n");
    }

}