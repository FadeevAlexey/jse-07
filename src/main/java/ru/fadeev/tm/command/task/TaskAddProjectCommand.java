package ru.fadeev.tm.command.task;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.exception.IllegalTaskNameException;
import ru.fadeev.tm.api.service.IProjectService;
import ru.fadeev.tm.api.service.ITaskService;
import ru.fadeev.tm.util.ConsoleHelper;

public final class TaskAddProjectCommand extends AbstractCommand {

    public TaskAddProjectCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "task-addProject";
    }

    @Override
    public String getDescription() {
        return "Adding task to the project.";
    }

    @Override
    public void execute() {
        final IProjectService projectService = serviceLocator.getProjectService();
        final ITaskService taskService = serviceLocator.getTaskService();
        final String currentUserId = serviceLocator.getAppStateService().getUser().getId();
        System.out.println("[ADD TASK TO PROJECT]");
        System.out.println("ENTER TASK NAME");
        final String taskId = taskService.findIdByName(ConsoleHelper.readString(), currentUserId);
        System.out.println("ENTER PROJECT NAME");
        final String projectId = projectService.findIdByName(ConsoleHelper.readString(), currentUserId);
        if (taskId == null || projectId == null)
            throw new IllegalTaskNameException("Can't find project or task");
        final Task task = taskService.findOne(taskId);
        task.setProjectId(projectId);
        taskService.merge(task);
        System.out.println("[OK]\n");
    }

}