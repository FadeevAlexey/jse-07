package ru.fadeev.tm.command.task;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.Task;

public final class TaskListCommand extends AbstractCommand {

    public TaskListCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() {
        final String currentUserId = serviceLocator.getAppStateService().getUser().getId();
        System.out.println("[TASK LIST]");
        int index = 1;
        for (final Task task : serviceLocator.getTaskService().findAll(currentUserId)) {
            System.out.println(index++ + ". " + task);
        }
        System.out.println();
    }

}