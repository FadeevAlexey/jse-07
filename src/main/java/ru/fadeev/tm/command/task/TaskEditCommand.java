package ru.fadeev.tm.command.task;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.exception.IllegalTaskNameException;
import ru.fadeev.tm.api.service.ITaskService;
import ru.fadeev.tm.util.ConsoleHelper;

import java.util.Date;

public final class TaskEditCommand extends AbstractCommand {

    public TaskEditCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "task-edit";
    }

    @Override
    public String getDescription() {
        return "Edit project.";
    }

    @Override
    public void execute() {
        final ITaskService taskService = serviceLocator.getTaskService();
        System.out.println("[TASK EDIT]");
        System.out.println("ENTER CURRENT NAME:");
        final String name = ConsoleHelper.readString();
        final String taskId = taskService.findIdByName(name, serviceLocator.getAppStateService().getUser().getId());
        if (taskId == null)
            throw new IllegalTaskNameException("Can't find task");
        final Task task = taskService.findOne(taskId);
        fillFields(task);
        taskService.merge(task);
        System.out.println("[OK]\n");
        System.out.println("WOULD YOU LIKE ADD PROJECT TO TASK ? USE COMMAND task-addProject\n");
    }

    private void fillFields(final Task task) {
        System.out.println("YOU CAN ADD EDIT DESCRIPTION OR PRESS ENTER");
        String description = ConsoleHelper.readString();
        System.out.println("YOU CAN ADD EDIT START DATE OR PRESS ENTER");
        final Date startDate = ConsoleHelper.readDate();
        System.out.println("YOU CAN ADD EDIT FINISH DATE OR PRESS ENTER");
        final Date finishDate = ConsoleHelper.readDate();
        if (description != null && !description.isEmpty()) task.setDescription(description);
        if (startDate != null) task.setStartDate(startDate);
        if (finishDate != null) task.setFinishDate(finishDate);
    }

}