package ru.fadeev.tm.command.task;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.exception.IllegalProjectNameException;
import ru.fadeev.tm.api.service.ITaskService;
import ru.fadeev.tm.util.ConsoleHelper;

public final class TaskRemoveCommand extends AbstractCommand {

    public TaskRemoveCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "task-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected tasks.";
    }

    @Override
    public void execute() {
        final ITaskService taskService = serviceLocator.getTaskService();
        System.out.println("[TASK REMOVE]");
        System.out.println("ENTER NAME");
        final String id = taskService.findIdByName(ConsoleHelper.readString(),
                serviceLocator.getAppStateService().getUser().getId());
        if (id == null)
            throw new IllegalProjectNameException("Can't find task");
        taskService.remove(id);
        System.out.println("[OK]\n");
    }

}