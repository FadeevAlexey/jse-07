package ru.fadeev.tm.command.task;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.exception.IllegalTaskNameException;
import ru.fadeev.tm.util.ConsoleHelper;

public final class TaskCreateCommand extends AbstractCommand {

    public TaskCreateCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "task-create";
    }

    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAppStateService().getUser().getId();
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        final String name = ConsoleHelper.readString();
        if (name == null || name.isEmpty())
            throw new IllegalTaskNameException("name can't be empty");
        final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        serviceLocator.getTaskService().persist(task);
        System.out.println("[OK]\n");
        System.out.println("WOULD YOU LIKE EDIT PROPERTIES TASK? USE COMMAND task-edit\n");
    }

}