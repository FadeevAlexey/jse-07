package ru.fadeev.tm.command.task;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.api.service.ServiceLocator;

public final class TaskClearCommand extends AbstractCommand {

    public TaskClearCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() {
        serviceLocator.getTaskService().removeAll(serviceLocator.getAppStateService().getUser().getId());
        System.out.println("[ALL TASKS REMOVE]\n");
    }

}