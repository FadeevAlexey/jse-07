package ru.fadeev.tm.command.application;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.User;

public final class HelpCommand extends AbstractCommand {

    public HelpCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean isPermission(final User user) {
        return true;
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Show all commands.";
    }

    @Override
    public void execute() {
        for (final AbstractCommand abstractCommand : serviceLocator.getAppStateService().getCommands())
            System.out.println(String.format("%s: %s",abstractCommand.getName(),abstractCommand.getDescription()));
    }

}