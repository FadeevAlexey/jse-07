package ru.fadeev.tm.command.application;

import com.jcabi.manifests.Manifests;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.User;

public final class AboutCommand extends AbstractCommand {

    public AboutCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean isPermission(final User user) {
        return true;
    }

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getDescription() {
        return "Product information.";
    }

    @Override
    public void execute() {
        System.out.println("*** ABOUT TASK MANAGER ***");
        System.out.print("Developer:" + Manifests.read("developer") + " ");
        System.out.println(Manifests.read("email"));
        System.out.println("Product: " + Manifests.read("artifactId"));
        System.out.println("Version: " + Manifests.read("version"));
        System.out.println("Build number: " + Manifests.read("buildNumber"));
        System.out.println("(C) 2020\n");
    }

}