package ru.fadeev.tm.api.repository;

import ru.fadeev.tm.entity.Project;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    void removeAll(final String userId);

    List<Project> findAll(final String userId);

    String findIdByName(final String name, final String userId);

}
