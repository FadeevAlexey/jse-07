package ru.fadeev.tm.api.repository;

import java.util.List;

public interface IRepository<E> {

    List<E> findAll();

    E findOne(final String id);

    E remove(final String id);

    E persist(final E e);

    E merge(final E e);

    void removeAll();

}