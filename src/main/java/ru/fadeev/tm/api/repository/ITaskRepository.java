package ru.fadeev.tm.api.repository;

import ru.fadeev.tm.entity.Task;

import java.util.Collection;

public interface ITaskRepository extends IRepository<Task> {

    void removeAll(final String userId);

    Collection<Task> findAll(final String userId);

    Collection<Task> findAllByProjectId(final String projectId);

    String findIdByName(final String name, final String userId);

}