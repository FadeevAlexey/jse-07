package ru.fadeev.tm.api.repository;

import ru.fadeev.tm.entity.User;

public interface IUserRepository extends IRepository<User> {

    boolean isLoginExist(final String login);

}