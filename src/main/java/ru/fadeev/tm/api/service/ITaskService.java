package ru.fadeev.tm.api.service;

import ru.fadeev.tm.entity.Task;

import java.util.Collection;

public interface ITaskService extends IService<Task> {

    void removeAll(final String userId);

    Collection<Task> findAll(final String userId);

    String findIdByName(final String name, final String userId);

    void removeAllByProjectId(final String projectId, final String userId);

    void removeAllProjectTask(final String userId);

}
