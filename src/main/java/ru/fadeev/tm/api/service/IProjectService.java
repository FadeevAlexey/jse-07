package ru.fadeev.tm.api.service;

import ru.fadeev.tm.entity.Project;

import java.util.List;

public interface IProjectService extends IService<Project> {

    String findIdByName(final String name, final String userId);

    List<Project> findAll(final String userId);

    void removeAll(final String userId);

}
