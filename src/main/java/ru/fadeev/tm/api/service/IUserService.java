package ru.fadeev.tm.api.service;

import ru.fadeev.tm.entity.User;

public interface IUserService extends IService<User> {

    boolean isLoginExist(final String login);

    User findUserByLogin(final String login);

}