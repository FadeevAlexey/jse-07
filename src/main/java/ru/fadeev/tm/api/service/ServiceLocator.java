package ru.fadeev.tm.api.service;

public interface ServiceLocator {

    IProjectService getProjectService();

    ITaskService getTaskService();

    IUserService getUserService();

    IAppStateService getAppStateService();

}