package ru.fadeev.tm.api.service;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;

import java.util.List;

public interface IAppStateService {

    Role getRole();

    User getUser();

    void setUser(final User user);

    void putCommand(final String description, final AbstractCommand abstractCommand);

    AbstractCommand getCommand(final String command);

    List<AbstractCommand> getCommands();

    boolean hasPermission(Role...roles);

}
