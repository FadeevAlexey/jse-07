package ru.fadeev.tm.api.service;

import java.util.List;

public interface IService<T> {

    List<T> findAll();

    T findOne(final String id);

    T remove(final String id);

    T persist(final T t);

    T merge(final T t);

    void removeAll();

}

