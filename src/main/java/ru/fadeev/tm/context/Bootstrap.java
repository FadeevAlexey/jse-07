package ru.fadeev.tm.context;

import ru.fadeev.tm.api.repository.IProjectRepository;
import ru.fadeev.tm.api.repository.ITaskRepository;
import ru.fadeev.tm.api.repository.IUserRepository;
import ru.fadeev.tm.api.service.*;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.command.application.AboutCommand;
import ru.fadeev.tm.command.application.ExitCommand;
import ru.fadeev.tm.command.application.HelpCommand;
import ru.fadeev.tm.command.project.*;
import ru.fadeev.tm.command.task.*;
import ru.fadeev.tm.command.user.*;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;
import ru.fadeev.tm.exception.AccessDeniedException;
import ru.fadeev.tm.exception.CommandCorruptException;
import ru.fadeev.tm.exception.IllegalCommandNameException;
import ru.fadeev.tm.repository.*;
import ru.fadeev.tm.service.*;
import ru.fadeev.tm.util.HashUtil;
import ru.fadeev.tm.util.ConsoleHelper;

public final class Bootstrap implements ServiceLocator {

    private final ITaskRepository taskRepository = new TaskRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IUserRepository userRepository = new UserRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IUserService userService = new UserService(userRepository);

    private final IAppStateService appStateService = new AppStateService();

    public void init() {
        registry(new ProjectCreateCommand(this));
        registry(new ProjectListCommand(this));
        registry(new ProjectRemoveCommand(this));
        registry(new ProjectClearCommand(this));
        registry(new ProjectEditCommand(this));
        registry(new ProjectTasksCommand(this));
        registry(new TaskCreateCommand(this));
        registry(new TaskListCommand(this));
        registry(new TaskRemoveCommand(this));
        registry(new TaskClearCommand(this));
        registry(new TaskEditCommand(this));
        registry(new TaskAddProjectCommand(this));
        registry(new UserLoginCommand(this));
        registry(new UserLogoutCommand(this));
        registry(new UserCreateCommand(this));
        registry(new UserEditCommand(this));
        registry(new UserUpdatePasswordCommand(this));
        registry(new UserProfileCommand(this));
        registry(new ExitCommand(this));
        registry(new HelpCommand(this));
        registry(new AboutCommand(this));
        initUser();
        start();
    }

    private void initUser() {
        final User user = new User("User");
        user.setPassword(HashUtil.stringToMd5Hash("user"));
        final User admin = new User("Admin");
        admin.setPassword(HashUtil.stringToMd5Hash("admin"));
        admin.setRole(Role.ADMINISTRATOR);
        userService.persist(user);
        userService.persist(admin);
    }

    private void start() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command = "";
        while (!"exit".equals(command)) {
            try {
                command = ConsoleHelper.readString();
                execute(command);
            } catch (final IllegalArgumentException e) {
                System.err.println("oops something went wrong");
            } catch (final RuntimeException e) {
                System.err.println(e.getMessage());
            }
        }
    }

    private void registry(final AbstractCommand command) {
        final String cliCommand = command.getName();
        final String cliDescription = command.getDescription();
        if (cliCommand == null || cliCommand.isEmpty())
            throw new CommandCorruptException();
        if (cliDescription == null || cliDescription.isEmpty())
            throw new CommandCorruptException();
        appStateService.putCommand(cliCommand, command);
    }

    private void execute(final String command) {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = getAppStateService().getCommand(command);
        if (abstractCommand == null)
            throw new IllegalCommandNameException("Wrong command name");
        if (!checkPermission(abstractCommand))
            throw new AccessDeniedException("Access denied");
        abstractCommand.execute();
    }

    private boolean checkPermission(final AbstractCommand abstractCommand) {
        final boolean isPermission = abstractCommand.isPermission(appStateService.getUser());
        if (abstractCommand.accessRole() == null)
            return isPermission;
        return isPermission && getAppStateService().hasPermission(abstractCommand.accessRole());
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAppStateService getAppStateService() {
        return appStateService;
    }

}