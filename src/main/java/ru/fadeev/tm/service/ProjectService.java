package ru.fadeev.tm.service;

import ru.fadeev.tm.api.repository.IProjectRepository;
import ru.fadeev.tm.api.service.IProjectService;
import ru.fadeev.tm.entity.Project;

import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Override
    public String findIdByName(final String name, final String userId) {
        if (name == null || name.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return projectRepository.findIdByName(name, userId);
    }

    @Override
    public List<Project> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return projectRepository.findAll(userId);
    }

    @Override
    public void removeAll(final String userId) {
        if (userId == null || userId.isEmpty()) return;
        projectRepository.removeAll(userId);
    }

}