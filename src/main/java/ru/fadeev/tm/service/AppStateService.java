package ru.fadeev.tm.service;

import ru.fadeev.tm.api.service.IAppStateService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;

import java.util.*;

public class AppStateService implements IAppStateService {

    private User user = null;

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Override
    public Role getRole() {
        return user == null ? null : user.getRole();
    }

    @Override
    public User getUser() {
        return user;
    }

    @Override
    public void setUser(final User user) {
        this.user = user;
    }

    @Override
    public void putCommand(final String description, final AbstractCommand abstractCommand){
        if (description == null || description.isEmpty()) return;
        if (abstractCommand == null) return;
        commands.put(description,abstractCommand);
    }

    @Override
    public AbstractCommand getCommand(final String command){
        if (command == null || command.isEmpty()) return null;
       return commands.get(command);
    }

    @Override
    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    @Override
    public boolean hasPermission(final Role...roles) {
        if (roles == null) return false;
        if (user == null || user.getRole() == null) return  false;
        final List<Role> roleList = Arrays.asList(roles);
        return roleList.contains(user.getRole());
    }

}