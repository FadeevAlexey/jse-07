package ru.fadeev.tm.service;

import ru.fadeev.tm.api.service.IUserService;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.exception.IllegalUserNameException;
import ru.fadeev.tm.api.repository.IUserRepository;

public final class UserService extends AbstractService<User> implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
       super(userRepository);
       this.userRepository = userRepository;
    }

    @Override
    public boolean isLoginExist(final String login) {
        if (login == null) return false;
        if (login.isEmpty()) throw new IllegalUserNameException("Login can't be empty");
        return userRepository.isLoginExist(login);
    }

    @Override
    public User findUserByLogin(final String login){
        if (login == null || login.isEmpty()) return null;
        return findAll().stream()
                .filter(usr -> usr.getName().equals(login))
                .findFirst().orElse(null);
    }

}