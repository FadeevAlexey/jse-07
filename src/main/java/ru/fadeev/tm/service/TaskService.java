package ru.fadeev.tm.service;

import ru.fadeev.tm.api.service.ITaskService;
import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.api.repository.ITaskRepository;

import java.util.Collection;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public void removeAll(final String userId) {
        if (userId == null || userId.isEmpty()) return;
        taskRepository.removeAll(userId);
    }

    @Override
    public Collection<Task> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return taskRepository.findAll(userId);
    }

    @Override
    public String findIdByName(final String name, final String userId) {
        if (name == null || name.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return taskRepository.findIdByName(name,userId);
    }

    @Override
    public void removeAllByProjectId(final String projectId, final String userId) {
        if (projectId == null || projectId.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        findAll(userId)
                .stream()
                .filter(task -> task.getProjectId().equals(projectId))
                .forEach(task -> remove(task.getId()));
    }

    public void removeAllProjectTask(final String userId){
        if (userId == null || userId.isEmpty()) return;
        findAll(userId)
                .forEach(task -> {
                    if (!task.getProjectId().isEmpty())
                        remove(task.getId());
                });
    }

}