package ru.fadeev.tm.service;

import ru.fadeev.tm.api.service.IService;
import ru.fadeev.tm.entity.AbstractEntity;
import ru.fadeev.tm.api.repository.IRepository;

import java.util.LinkedList;
import java.util.List;

public class AbstractService<E extends AbstractEntity> implements IService<E> {

    private final IRepository<E> repository;

    public AbstractService(final IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public List<E> findAll() {
        return new LinkedList<>(repository.findAll());
    }

    @Override
    public E findOne(final String id) {
        if (id == null || id.isEmpty()) return null;
        return repository.findOne(id);
    }

    @Override
    public E remove(final String id) {
        if (id == null || id.isEmpty()) return null;
        return repository.remove(id);
    }

    @Override
    public E persist(final E e) {
        if (e == null) return null;
        return repository.persist(e);
    }

    @Override
    public E merge(final E e) {
        if (e == null) return null;
        return repository.merge(e);
    }

    @Override
    public void removeAll() {
        repository.removeAll();
    }

}