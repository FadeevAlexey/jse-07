package ru.fadeev.tm.entity;

import ru.fadeev.tm.enumerated.Role;

public final class User extends AbstractEntity {

    private String name;

    private String password = "";

    private Role role = Role.USER;

    public User(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public void setRole(final Role role) {
        this.role = role;
    }

    public Role getRole() {
        return role;
    }

    @Override
    public String toString() {
        return "name = " + name + '\'' +
                ", role=" + role.getGetDisplayName() +
                ", id='" + getId() + '\'' +
                '}';
    }

}