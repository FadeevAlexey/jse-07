package ru.fadeev.tm.entity;

import ru.fadeev.tm.util.ConsoleHelper;

import java.util.Date;

public final class Task extends AbstractEntity {

    private String name = "";

    private String description = "";

    private String projectId = "";

    private Date startDate;

    private Date finishDate;

    private String userId;

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public void setStartDate(final Date startDate) {
        this.startDate = startDate;
    }

    public void setFinishDate(final Date finishDate) {
        this.finishDate = finishDate;
    }

    public void setProjectId(final String projectId) {
        this.projectId = projectId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Task{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", startDate=" + ConsoleHelper.dateToString(startDate) +
                ", finishDate=" + ConsoleHelper.dateToString(finishDate) +
                '}';
    }

}