package ru.fadeev.tm.enumerated;

public enum Role {

    USER("User"),
    ADMINISTRATOR("Administrator");

    private final String getDisplayName;

    Role(final String getDisplayName) {
        this.getDisplayName = getDisplayName;
    }

    public String getGetDisplayName() {
        return getDisplayName;
    }

}